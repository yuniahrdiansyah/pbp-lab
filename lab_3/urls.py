from django.urls import path
from .views import index, addFriendy

urlpatterns = [
    path('', index, name='index'),
    # TODO Add friends path using friend_list Views --already done
    path('add/', addFriendy, name='add')
]
