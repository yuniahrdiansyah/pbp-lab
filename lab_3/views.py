from django.shortcuts import render
from lab_1.models import Friend
from .forms import FriendForm
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all() # TODO Implement this -done
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def addFriendy(request):
    response = {}
    forms = FriendForm(request.POST or None)
    if request.method == "POST" :
        if forms.is_valid():
            forms.save()
            return HttpResponseRedirect('/lab-3')
    response['forms'] = forms
    return render(request,'lab3_forms.html', response)