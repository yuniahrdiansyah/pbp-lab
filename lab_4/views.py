from django.shortcuts import render
from lab_2.models import Note
from lab_4.forms import NoteForm
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    response = {"data" : Note.objects.all}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    response = {}
    forms = NoteForm(request.POST or None)
    if request.method == "POST" :
        if forms.is_valid():
            forms.save()
            return HttpResponseRedirect('/lab-4')
    response['forms'] = forms
    return render(request,'lab4_form.html', response)

def note_list(request):
    response = {"data" : Note.objects.all}
    return render(request, 'lab4_note_list.html', response)