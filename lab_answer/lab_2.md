Selamat Pagi/Siang/Sore/Malam Bapak Ibu Dosen PBP dan kakak asisten dosen.
Izinkan saya, Yunia Harmulyati dengan NPM 2106750515 menjawab tugas ini,

1. Apakah perbedaan antara JSON dan XML?
Jawaban :
Sebelum menjawab, ada baiknya kita mengerti pengertian dari JSON dan XML terlebih dahulu. 

JSON atau JavaScript Object Notation adalah sebuah format data yang digunakan untuk pertukaran dan penyimpanan data. JSON sendiri merupakan bagian atau subset dari Javascript. Seperti yang kita ketahui bahwa JSON dapat dibaca menggunakan bahasa pemrograman seperti C, C++, Java, dan lainnya. Hal ini membuktikan bahwa JSON merupakan bahasa ideal untuk pertukaran data antar aplikasi.

XML atau eXtensible Markup Language adalah sebuah bahasa markup seperti HTML yang didesain untuk menyimpan dan mengantarkan data. Dalam XML, nama tag dapat diciptakan sendiri. Hal ini merupakan salah satu perbedaan antara XML dengan HTML yang memiliki nama tag yang sudah baku. 

Menjawab pertanyaan pertama, perbedaan yang saya temukan adalah :

a) JSON adalah (Notasi Objek JavaScript) adalah standar terbuka berbasis teks untuk pertukaran data. Sementara XML (bahasa markup yang peka) adalah format independen perangkat lunak-perangkat keras untuk pertukaran data.
b) Pada tipe, JSON adalah bahasa meta. Sementara XML adalah bahasa markup.
c) Pada Kompleksitas, JSON sederhana dan mudah dibaca. Sementara XML lebih rumit.
d) Pada Orientasi, JSON berorientasi data. Sementara XML berorientasi pada dokumen.
e) Pada Array, JSON mendukung array. Sementara XML tidak mendukung array.
f) Pada Ekstensi File, File JSON diakhiri dengan ekstensi .json. Semntara, File XML diakhiri dengan ekstensi .xml.

Kesimpulan yang bisa saya dapatkan adalah JSON dan XML merupakan teknologi web yang populer. JSON ringan, sederhana dan mudah dibaca. Di sisi lain, XML lebih bisa dikembangkan dan rumit. Perbedaan antara JSON dan XML adalah bahwa JSON adalah bahasa meta dan XML adalah bahasa markup. Umumnya, JSON lebih disukai daripada XML karena XML diperkeras untuk mengurai daripada JSON. Ini lebih cepat dan lebih mudah daripada XML dalam aplikasi terkait AJAX. JSON dapat digunakan sebagai alternatif XML.

2. Apakah perbedaan antara HTML dan XML?
Jawaban :

Seperti di nomor sebelumnya, kita akan mengulas kembali pengertian dari HTML dan XMl terlebih dahulu. Mengingat pada nomor 1 telah dijelaskan pengertian dari XML. Maka, saya akan menjelaskan pengertian HTML saja. 

HTML atau Hypertext Markup Language adalah bahasa markup yang digunakan untuk membuat struktur halaman website. HTML terdiri dari kombinasi teks dan simbol yang disimpan dalam sebuah file. Dalam membuat file HTML, terdapat standar atau format khusus yang harus diikuti. Format tersebut telah tertuang dalam standar kode internasional atau ASCII (American Standard Code for Information Interchange). 

Menjawab pertanyaan kedua, perbedaan yang saya temukan adalah :

a) XML adalah bahasa markup berbasis teks yang memiliki struktur yang menggambarkan diri sendiri dan secara efektif dapat mendefinisikan bahasa markup lainnya. Sementara, HTML adalah bahasa markup yang telah ditentukan dan memiliki kemampuan terbatas.
b) XML menyediakan penataan logis dokumen. Sementara, struktur HTML ditentukan sebelumnya di mana tag "head" dan "body" digunakan.
c) HTML dirancang dengan penekanan pada fitur penyajian data. Sebaliknya, XML adalah data spesifik di mana penyimpanan dan transfer data menjadi perhatian utama.
d) Spasi putih dalam XML digunakan untuk penggunaan khusus karena XML mempertimbangkan setiap karakter tunggal. Sebaliknya, HTML dapat mengabaikan spasi putih.
e) Tag dalam XML wajib ditutup. Sementara, dalam HTML tag terbuka juga dapat berfungsi dengan baik.

Kesimpulan yang dapat ditarik adalah bahasa markup XML dan HTML saling terkait satu sama lain di mana HTML digunakan untuk presentasi data sedangkan tujuan utama XML adalah untuk menyimpan dan mentransfer data. HTML adalah bahasa yang sederhana dan sudah ditentukan sebelumnya, sedangkan XML adalah bahasa markup standar untuk mendefinisikan bahasa lain. Penguraian dokumen XML mudah dan cepat.

Sekian yang dapat saya sampaikan. Untuk kurang dan salahnya mohon dimaafkan dan dikoreksi. Terima kasih.

Salam,
Yunia Harmulyati

Sumber referensi :
https://www.petanikode.com/ diakses pada 26 September 2021
https://id.strephonsays.com/ diakses pada 26 September 2021
https://www.niagahoster.co.id/ diakses pada 26 September 2021
https://id.gadget-info.com/ diakses pada 26 September 2021




