from django.shortcuts import render
from datetime import datetime, date
from .models import Friend

mhs_name = 'Yunia Harmulyati'  # TODO Implement this --done
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2003,6,27)  # TODO Implement this, format (Year, Month, Date) -done
npm = 2106750515 # TODO Implement this -done


def index(request):
    response = {'name': mhs_name,
                'age': calculate_age(birth_date.year),
                'npm': npm}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0


def friend_list(request):
    friends = Friend.objects.all() # TODO Implement this -done
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)
